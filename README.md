# Time_Tracker_Ruby

**Summary**
This is a command line interface tool for logging time. Employees will be able to use the application to log their time throughout the week. The data for the application are persisted to the file system.  Employees can request to run a report on the current month of time logged.
