require 'messages_to_console'
require 'messages'
require 'input_to_console'
require 'search'

RSpec.describe MessagesToConsole do
  let(:search) { Search.new(search_type)}
  let(:messages) { Messages.new }
  let(:input_to_console) { InputToConsole.new }
  let(:messages_to_console) {
    obj = double()
    allow(obj).to receive(:print_message).with(messages.welcome_user_message)
    .and_return("Welcome to Sundial!")
   }

  describe '#welcome_user_message' do
    it "returns a formatted string to greet the user" do

      expect(messages.welcome_user_message)
      .to eq("\nWelcome to Sundial!\n")
   end
 end

 describe '#request_username' do
   it "returns a formatted string to request the username" do

     expect(messages.request_username)
     .to eq("\nPlease enter your username\n")
  end
end

describe '#request_date' do
  it "returns a formatted string to request the date" do

    expect(messages.request_date)
    .to eq("\nPlease enter the date you want to log time for (MM/DD/YYYY)\n")
  end
end

describe '#request_worked_hours' do
  it "returns a formatted string to request the hours worked" do

    expect(messages.request_worked_hours)
    .to eq("\nPlease enter the hours worked for the date you specified\n")
  end
end

describe '#request_timecode' do
  it "returns a formatted string to request the timecode" do

    expect(messages.request_timecode)
    .to eq("\nPlease enter the timecode you want to log time for. \n\nYou can choose from one of the following: \nBillable work \nNon-billable work \nPTO\n\n")
  end
end

describe '#request_client' do
  it "returns a formatted string to request the client" do

    expect(messages.request_client)
    .to eq("\nPlease enter the client you want to log time for.\n\n")
  end
end

describe '#display_main_menu_options' do
  it "returns a formatted string" do

    expect(messages.display_main_menu_options)
    .to eq("\nPlease type a number to complete an action from the following options: \n1. Log time for another date \n2. Generate a Report \n3. Exit the application\n\n")
  end
end

describe '#display_main_menu_options' do
  it "returns a formatted string" do

    expect(messages.display_main_menu_options)
    .to eq("\nPlease type a number to complete an action from the following options: \n1. Log time for another date \n2. Generate a Report \n3. Exit the application\n\n")
  end
end

describe '#validate_client' do
  it "returns a formatted string" do

    expect(messages.validate_client)
    .to eq("\nThis is not a client from the list, please try again\n\n")
  end
end

describe '#validate_date' do
  it "returns a formatted string" do

    expect(messages.validate_date)
    .to eq("\nThat is not a valid date, please try again\n\n")
  end
end

describe '#validate_date' do
  it "returns a formatted string" do

    expect(messages.validate_date)
    .to eq("\nThat is not a valid date, please try again\n\n")
  end
end

describe '#validate_menu_option' do
  it "returns a formatted string" do

    expect(messages.validate_menu_option)
    .to eq("\nThis is not a valid choice, please try again\n\n")
  end
end

describe '#validate_timecode' do
  it "returns a formatted string" do

    expect(messages.validate_timecode)
    .to eq("\nThis is not a valid timecode, please try again\n\n")
  end
end

describe '#validate_hours_worked' do
  it "returns a formatted string" do

    expect(messages.validate_hours_worked)
    .to eq("\nThis is not a valid number of hours, please try again\n\n")
  end
end

describe '#check_for_empty_line' do
  it "returns a formatted string" do

    expect(messages.check_for_empty_line)
    .to eq("\nThis is not a valid response, please try again\n\n")
  end
end

describe '#no_user_input' do
  it "returns a formatted string" do

    expect(messages.no_user_input)
    .to eq("\nYou did not type an answer. Please type your answer and press enter.\n\n")
  end
end

describe '#display_total_pto_hours' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::PTO_ONLY_SEARCH).results

    expect(messages.display_total_pto_hours(total_hours))
    .to eq("Here are your total PTO hours: #{total_hours}.")
  end
end

describe '#display_total_billable_hours' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::BILLABLE_ONLY_SEARCH).results

    expect(messages.display_total_billable_hours(total_hours))
    .to eq("Here are your total Billable hours: #{total_hours}.")
  end
end

describe '#display_total_non_billable_hours' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::NON_BILLABLE_ONLY_SEARCH).results

    expect(messages.display_total_non_billable_hours(total_hours))
    .to eq("Here are your total Non-billable hours: #{total_hours}.")
  end
end

describe '#display_hours_worked_on_andys_eatery' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::FILTER_BY_ANDYS_EATERY).results

    expect(messages.display_hours_worked_on_andys_eatery(total_hours))
    .to eq("\nHere are the total hours you worked for Andy's Eatery: #{total_hours}.")
  end
end

describe '#display_hours_worked_on_cyrus_physics_outlet' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::FILTERY_BY_CYRUS_PHYSICS_OUTLET).results

    expect(messages.display_hours_worked_on_cyrus_physics_outlet(total_hours))
    .to eq("\nHere are the total hours you worked for Cyrus' Physics Outlet: #{total_hours}.")
  end
end

describe '#display_hours_worked_on_erics_emporium_board_game' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::FILTER_BY_ERICS_BOARD_GAME_EMPORIUM).results

    expect(messages.display_hours_worked_on_erics_emporium_board_game(total_hours))
    .to eq("\nHere are the total hours you worked for Eric's Board Game Emporium: #{total_hours}.")
  end
end

describe '#display_hours_worked_on_katrinas_armory' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::FILTER_BY_KATRINAS_ARMORY).results

    expect(messages.display_hours_worked_on_katrinas_armory(total_hours))
    .to eq("\nHere are the total hours you worked for Katrina's Armory: #{total_hours}.")
  end
end

describe '#display_hours_worked_on_pixel_perfect' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::FILTER_BY_PIXEL_PERFECT).results

    expect(messages.display_hours_worked_on_pixel_perfect(total_hours))
    .to eq("\nHere are the total hours you worked for Pixel Perfect: A Puppy Photo Boutieque: #{total_hours}.")
  end
end

describe '#display_hours_worked_on_tams_deluxe_dancewear' do
  it "returns a formatted string to request the timecode" do
    total_hours = Search.new(Search::FILTER_BY_TAMS_DELUXE_DANCEWEAR).results

    expect(messages.display_hours_worked_on_tams_deluxe_dancewear(total_hours))
    .to eq("\nHere are the total hours you worked for Tam's Deluxe Dancewear: #{total_hours}.")
  end
end

end
