require 'validate'
require 'input_to_console'

RSpec.describe Validate do
  let(:validate) { Validate.new }
  let(:messages) { Messages.new }
  let(:messages_to_console) { MessagesToConsole.new }
  let(:input_to_console) { InputToConsole.new }

  describe "#valid_date?" do
    it "validates the date" do

      user_input = "2019/01/08"

      expect(validate.valid_date?(user_input, messages)).to eq("\nThat is not a valid date, please try again\n\n")
    end

    it "validates the date" do

      user_input = '01/03/2019'

      expect(validate.valid_date?(user_input, messages)).to eq(true)
    end
  end

  describe "#validate_hours_worked" do
    it "validates the hours worked" do

      user_input = 2019

      expect(validate.validate_hours_worked(user_input, messages)).to eq("\nThis is not a valid number of hours, please try again\n\n")
    end

    it "validates the hours worked" do

      user_input = 2

      expect(validate.validate_hours_worked(user_input, messages)).to eq(nil)
    end
  end

  describe '#validate_timecode' do
    it "validates the timecode" do
      user_input = "client work"

      expect(validate.validate_timecode(user_input, messages)).to eq("\nThis is not a valid timecode, please try again\n\n")
    end

    it "validates the timecode" do
      user_input = "billable work"

      expect(validate.validate_timecode(user_input, messages)).to eq(nil)
    end
  end

  describe '#validate_client' do
    it "validates the client" do

      user_input = "Tessa's Coding Services Inc"
      expect(validate.validate_client(user_input, messages)).to eq("\nThis is not a client from the list, please try again\n\n")
    end

    it "validates the client" do

      user_input = "Andy's Columbian Eatery"
      expect(validate.validate_client(user_input, messages)).to eq(nil)
    end
  end

  describe '#validate_menu_option_selected' do
    it "validates the menu option selected" do
      user_input = "1"

      expect(validate.validate_menu_option_selected(user_input, messages)).to eq(nil)
    end

    it "validates the menu option selected" do
      user_input = "2"

      expect(validate.validate_menu_option_selected(user_input, messages)).to eq(nil)
    end

    it "validates the menu option selected" do
      user_input = "3"

      expect(validate.validate_menu_option_selected(user_input, messages)).to eq("\nThis is not a valid choice, please try again\n\n")
    end
  end

  describe '#check_for_empty_line' do
    it "validates the input is not blank" do
      user_input = "\n"

      expect(validate.check_for_empty_line(user_input, messages)).to eq("\nThis is not a valid response, please try again\n\n")
    end
  end
end
