require 'messages_to_console'
require 'messages'
require 'time_tracker_runner'
require 'input_to_console'
require 'billable_client_list'

RSpec.describe TimeTrackerRunner do
  let(:messages) { Messages.new }
  let(:messages_to_console) { MessagesToConsole.new }
  let(:input_to_console) { InputToConsole.new }
  let(:employee_file_writer) { EmployeeFileWriter.new }
  let(:billable_client_list) { BillableClientList.new }
  let(:time_tracker_runner) { TimeTrackerRunner.new(messages_to_console, messages, input_to_console, employee_file_writer, billable_client_list) }

  xdescribe "#write_to_employee_file" do
    it "returns input as text to a file" do

      allow(input_to_console)
      .to receive(:gets)
      .and_return("Sundial11")

      expect(time_tracker_runner.write_to_employee_file)
      .to eq("Sundial11")
    end
  end
end
