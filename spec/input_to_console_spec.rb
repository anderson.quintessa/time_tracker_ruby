require 'input_to_console'
require 'date'

RSpec.describe InputToConsole do
  let(:input_to_console) { InputToConsole.new }

  describe "#get_user_input" do
    it "returns input as a string" do

      allow(input_to_console)
      .to receive(:gets)
      .and_return("Cyrus1")

      expect(input_to_console.get_user_input).to eq("cyrus1")
    end
  end

  describe "#get_user_input" do
    it "returns input as a string" do

      allow(input_to_console)
      .to receive(:gets)
      .and_return("Miss_Tessa11")

      expect(input_to_console.get_user_input).to eq("miss_tessa11")
    end
  end

  describe "#get_user_input" do
    it "returns input as a string" do

      allow(input_to_console)
      .to receive(:gets)
      .and_return("01/01/2019")

      expect(input_to_console.get_user_input).to eq("01/01/2019")
    end
  end
end
