require 'messages_to_console'
require 'messages'
require 'time_tracker_runner'
require 'input_to_console'
require 'billable_client_list'
require 'validate'

RSpec.describe TimeTrackerRunner do
  let(:messages) { Messages.new }
  let(:messages_to_console) { MessagesToConsole.new }
  let(:input_to_console) { InputToConsole.new }
  let(:employee_file_writer) { EmployeeFileWriter.new }
  let(:billable_client_list) { BillableClientList.new }
  let(:validate) { Validate.new }
  let(:time_tracker_runner) { TimeTrackerRunner.new(messages_to_console, messages, input_to_console, employee_file_writer, billable_client_list, validate) }

  xdescribe '#get_billable_client_list' do #need to fix: outputs assertion to the console
    it "returns an array of strings" do

      expect(billable_client_list.read_billable_client_list)
      .to eq(["Andy's Columbian Eatery\n", "Cyrus' Physics Outlet\n", "Eric's Board Game Emporium\n", "Katrina's Armory\n", "Pixel Perfect: A Puppy Photo Boutique\n", "Tam's Deluxe Dancewear\n"])
    end
  end

  xdescribe '#validate_client' do
    it "handles invalid input" do

      expect(billable_client_list.validate_client).to eq("This is not a client from the list, please try again")
    end
  end
end
