require 'search'
require 'pry'

RSpec.describe Search do
  search_type = 0

  let(:search) { Search.new(search_type) }

  describe '#get_pto_hours_only' do
    it "returns an integer" do
      entry = [["02/08/2019", "Vielka", "8", "PTO"], ["04/16/2019", "Mohammad", "23", "Billable work"],["01/29/2019", "Vielka", "3", "PTO"]]
      name = "Vielka"

      expect(search.get_pto_hours_only(entry, name))
      .to eq(11)
    end

    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work"],["01/29/2019", "Vielka", "3", "PTO"], ["10/19/2018", "Haley", "7", "Non-billable work"], ["07/27/2019", "Reese", "18", "PTO"]]
      name = "Vielka"

      expect(search.get_pto_hours_only(entry, name))
      .to eq(3)
    end

    it "returns an integer" do
      entry = [["02/08/2019", "Vielka", "8", "PTO"], ["04/16/2019", "Mohammad", "23", "PTO"],["01/29/2019", "Vielka", "3", "PTO"]]
      name = "Mohammad"

      expect(search.get_pto_hours_only(entry, name))
      .to eq(23)
    end

    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work"],["01/29/2019", "Vielka", "3", "PTO"], ["10/19/2018", "Haley", "7", "Non-billable work"], ["07/27/2019", "Reese", "18", "PTO"]]
      name = "Reese"

      expect(search.get_pto_hours_only(entry, name))
      .to eq(18)
    end
  end

  describe '#get_non_billable_hours_only' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work"],["01/29/2019", "Vielka", "3", "PTO"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Haley"

      expect(search.get_non_billable_hours_only(entry, name))
      .to eq(7)
    end

    it "returns an integer" do
      entry = [["04/16/2019", "Haley", "23", "Non-billable work"],["01/29/2019", "Vielka", "3", "PTO"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Haley"

      expect(search.get_non_billable_hours_only(entry, name))
      .to eq(30)
    end
  end

  describe '#get_billable_hours_only' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work"],["01/29/2019", "Vielka", "3", "PTO"], ["05/17/2020", "Justine", "12", "Billable work"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Mohammad"

      expect(search.get_billable_hours_only(entry, name))
      .to eq(23)
    end

    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work"], ["05/17/2020", "Justine", "15", "Billable work"], ["01/29/2019", "Vielka", "3", "PTO"], ["05/17/2020", "Justine", "12", "Billable work"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Justine"

      expect(search.get_billable_hours_only(entry, name))
      .to eq(27)
    end
  end

  describe '#filter_by_name' do
    it "filters by name" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work"],["01/29/2019", "Vielka", "3", "PTO"]]
      name = "Mohammad"

      expect(search.filter_by_name(entry, name))
      .to eq([["04/16/2019", "Mohammad", "23", "Billable work"]])
    end
  end

  describe '#get_hours_worked_on_andys_eatery' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work", "Andy's Columbian Eatery"],["01/29/2019", "Vielka", "3", "PTO"], ["05/17/2020", "Justine", "12", "Billable work", "andy's columbian eatery"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Mohammad"
      client_name = "Andy's Columbian Eatery"

      expect(search.get_hours_worked_on_andys_eatery(entry, name, client_name))
      .to eq(23)
    end
  end

  describe '#get_hours_worked_on_cyrus_physics_outlet' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work", "Andy's Columbian Eatery"],["01/29/2019", "Vielka", "3", "Billable work", "Cyrus' Physics_Outlet"], ["05/17/2020", "Justine", "12", "Billable work", "Andy's Columbian Eatery"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Vielka"
      client_name = "Cyrus' Physics_Outlet"

      expect(search.get_hours_worked_on_cyrus_physics_outlet(entry, name, client_name))
      .to eq(3)
    end
  end

  describe '#get_hours_worked_on_erics_emporium_board_game' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work", "Eric's Emporium Board Game"],["01/29/2019", "Vielka", "3", "Billable work", "Cyrus' Physics_Outlet"], ["05/17/2020", "Justine", "12", "Billable work", "Andy's Columbian Eatery"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Mohammad"
      client_name = "Eric's Emporium Board Game"

      expect(search.get_hours_worked_on_erics_emporium_board_game(entry, name, client_name))
      .to eq(23)
    end
  end

  describe '#get_hours_worked_on_katrinas_armory' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work", "Andy's Columbian Eatery"],["01/29/2019", "Vielka", "3", "Billable work", "Cyrus' Physics_Outlet"], ["05/17/2020", "Justine", "12", "Billable work", "Katrina's Armory"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Justine"
      client_name = "Katrina's Armory"

      expect(search.get_hours_worked_on_katrinas_armory(entry, name, client_name))
      .to eq(12)
    end
  end

  describe '#get_hours_worked_on_pixel_perfect' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work", "Andy's Columbian Eatery"],["01/29/2019", "Vielka", "3", "Billable work", "Pixel Perfect: A Puppy Photo Boutique"], ["05/17/2020", "Justine", "12", "Billable work", "andy's columbian eatery"], ["10/19/2018", "Haley", "7", "Non-billable work"]]
      name = "Vielka"
      client_name = "Pixel Perfect: A Puppy Photo Boutique"

      expect(search.get_hours_worked_on_pixel_perfect(entry, name, client_name))
      .to eq(3)
    end
  end

  describe '#get_hours_worked_on_tams_deluxe_dancewear' do
    it "returns an integer" do
      entry = [["04/16/2019", "Mohammad", "23", "Billable work", "Andy's Columbian Eatery"],["01/29/2019", "Vielka", "3", "Billable work", "Cyrus' Physics_Outlet"], ["05/17/2020", "Justine", "12", "Billable work", "andy's columbian eatery"], ["10/19/2018", "Haley", "7", "Billable work", "Tam's Deluxe Dancewear"]]
      name = "Haley"
      client_name = "Tam's Deluxe Dancewear"

      expect(search.get_hours_worked_on_tams_deluxe_dancewear(entry, name, client_name))
      .to eq(7)
    end
  end

end
