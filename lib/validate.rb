require 'date'

class Validate

  def valid_date?(date, messages)
    date_format = '%m/%d/%Y'
    DateTime.strptime(date, date_format)
    true
  rescue ArgumentError
    messages.validate_date
  end

  def validate_hours_worked(data_to_validate, messages)
    hours = (0..24)
    unless hours.include?(data_to_validate)
      messages.validate_hours_worked
    end
  end

  def validate_timecode(data_to_validate, messages)
    unless data_to_validate == "billable work" || data_to_validate == "non-billable work" || data_to_validate == "pto"
      messages.validate_timecode
    end
  end

  def validate_client(data_to_validate, messages)
    unless data_to_validate == "Andy's Columbian Eatery" || data_to_validate == "Cyrus' Physics Outlet" || data_to_validate == "Eric's Board Game Emporium" || data_to_validate == "Katrina's Armory" || data_to_validate == "Pixel Perfect: A Puppy Photo Boutique" || data_to_validate == "Tam's Deluxe Dancewear"
      messages.validate_client
    end
  end

  def validate_menu_option_selected(data_to_validate, messages)
    unless data_to_validate == "1" || data_to_validate == "2"
      messages.validate_menu_option
    end
  end

  def check_for_empty_line(data_to_validate, messages)
    if data_to_validate == "\n"
      messages.check_for_empty_line
    end
  end
end
