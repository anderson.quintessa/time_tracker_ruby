require_relative 'search'

class MessagesToConsole

  def print_message(messages)
    puts messages
  end

  def welcome_user(messages)
    print_message(messages.welcome_user_message)
  end

  def request_username(messages)
    print_message(messages.request_username)
  end

  def request_date(messages)
    print_message(messages.request_date)
  end

  def request_worked_hours(messages)
    print_message(messages.request_worked_hours)
  end

  def request_timecode(messages)
    print_message(messages.request_timecode)
  end

  def request_client(messages)
    print_message(messages.request_client)
  end

  def display_main_menu_options(messages)
    print_message(messages.display_main_menu_options)
  end

  def format_client_list(messages)
    print_messages(messages.format_client_list)
  end

  def validate_date(messages)
    print_message(messages.validate_date)
  end

  def validate_timecode(messages)
    print_message(messages.validate_timecode)
  end

  def validate_hours_worked(messages)
    print_message(messages.validate_hours_worked)
  end

  def validate_client(messages)
    print_message(messages.validate_client)
  end

  def validate_menu_option(messages)
    print_message(messages.validate_menu_option)
  end

  def check_for_empty_line(messages)
    print_message(messages.check_for_empty_line)
  end

  def display_total_pto_hours(messages)
    total_hours = Search.new(Search::PTO_ONLY_SEARCH).results
    print_message(messages.display_total_pto_hours(total_hours))
  end

  def display_total_billable_hours(messages)
    total_hours = Search.new(Search::BILLABLE_ONLY_SEARCH).results
    print_message(messages.display_total_billable_hours(total_hours))
  end

  def display_total_non_billable_hours(messages)
    total_hours = Search.new(Search::NON_BILLABLE_ONLY_SEARCH).results
    print_message(messages.display_total_non_billable_hours(total_hours))
  end

  def display_hours_worked_on_andys_eatery(messages)
    total_hours = Search.new(Search::FILTER_BY_ANDYS_EATERY).results
    print_message(messages.display_hours_worked_on_andys_eatery(total_hours))
  end

  def display_hours_worked_on_cyrus_physics_outlet(messages)
    total_hours = Search.new(Search::FILTERY_BY_CYRUS_PHYSICS_OUTLET).results
    print_message(messages.display_hours_worked_on_cyrus_physics_outlet(total_hours))
  end

  def display_hours_worked_on_erics_emporium_board_game(messages)
    total_hours = Search.new(Search::FILTER_BY_ERICS_BOARD_GAME_EMPORIUM).results
    print_message(messages.display_hours_worked_on_erics_emporium_board_game(total_hours))
  end

  def display_hours_worked_on_katrinas_armory(messages)
    total_hours = Search.new(Search::FILTER_BY_KATRINAS_ARMORY).results
    print_message(messages.display_hours_worked_on_katrinas_armory(total_hours))
  end

  def display_hours_worked_on_pixel_perfect(messages)
    total_hours = Search.new(Search::FILTER_BY_PIXEL_PERFECT).results
    print_message(messages.display_hours_worked_on_pixel_perfect(total_hours))
  end

  def display_hours_worked_on_tams_deluxe_dancewear(messages)
    total_hours = Search.new(Search::FILTER_BY_TAMS_DELUXE_DANCEWEAR).results
    print_message(messages.display_hours_worked_on_tams_deluxe_dancewear(total_hours))
  end

end
