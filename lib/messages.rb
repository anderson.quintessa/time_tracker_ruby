class Messages
  def welcome_user_message
    "\nWelcome to Sundial!\n"
  end

  def request_username
    "\nPlease enter your username\n"
  end

  def request_date
    "\nPlease enter the date you want to log time for (MM/DD/YYYY)\n"
  end

  def request_worked_hours
    "\nPlease enter the hours worked for the date you specified\n"
  end

  def request_timecode
    "\nPlease enter the timecode you want to log time for. \n\nYou can choose from one of the following: \nBillable work \nNon-billable work \nPTO\n\n"
  end

  def request_client
    "\nPlease enter the client you want to log time for.\n\n"
  end

  def display_main_menu_options
      "\nPlease type a number to complete an action from the following options: \n1. Log time for another date \n2. Generate a Report \n3. Exit the application\n\n"
  end

  def no_user_input
    "\nYou did not type an answer. Please type your answer and press enter.\n\n"
  end

  def format_client_list(line)
    "\n" << line << "\n"
  end

  def validate_client
    "\nThis is not a client from the list, please try again\n\n"
  end

  def validate_date
    "\nThat is not a valid date, please try again\n\n"
  end

  def validate_menu_option
      "\nThis is not a valid choice, please try again\n\n"
  end

  def validate_timecode
    "\nThis is not a valid timecode, please try again\n\n"
  end

  def validate_hours_worked
    "\nThis is not a valid number of hours, please try again\n\n"
  end

  def check_for_empty_line
    "\nThis is not a valid response, please try again\n\n"
  end

  def display_total_pto_hours(total_hours)
    "Here are your total PTO hours: #{total_hours}."
  end

  def display_total_billable_hours(total_hours)
    "Here are your total Billable hours: #{total_hours}."
  end

  def display_total_non_billable_hours(total_hours)
    "Here are your total Non-billable hours: #{total_hours}."
  end

  def display_hours_worked_on_andys_eatery(total_hours)
    "\nHere are the total hours you worked for Andy's Eatery: #{total_hours}."
  end

  def display_hours_worked_on_cyrus_physics_outlet(total_hours)
    "\nHere are the total hours you worked for Cyrus' Physics Outlet: #{total_hours}."
  end

  def display_hours_worked_on_erics_emporium_board_game(total_hours)
    "\nHere are the total hours you worked for Eric's Board Game Emporium: #{total_hours}."
  end

  def display_hours_worked_on_katrinas_armory(total_hours)
    "\nHere are the total hours you worked for Katrina's Armory: #{total_hours}."
  end

  def display_hours_worked_on_pixel_perfect(total_hours)
    "\nHere are the total hours you worked for Pixel Perfect: A Puppy Photo Boutieque: #{total_hours}."
  end

  def display_hours_worked_on_tams_deluxe_dancewear(total_hours)
    "\nHere are the total hours you worked for Tam's Deluxe Dancewear: #{total_hours}."
  end
end
