class EmployeeFileWriter

  def write_to_file(date, username, hours, search_type, client = "")
    File.open("employee_data.txt", "a") do |line|
      line.print "#{date}|#{username}|#{hours}|#{search_type}|#{client}\n"
    end
  end
end
