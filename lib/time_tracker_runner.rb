require_relative 'messages'
require_relative 'messages_to_console'
require_relative 'employee_file_writer'
require_relative 'billable_client_list'


class TimeTrackerRunner

  attr_reader :messages_to_console, :messages, :billable_client_list, :validate
  attr_accessor :input_to_console, :employee_file_writer, :answer, :username, :date, :hours_worked, :timecode, :client

  def initialize(messages_to_console, messages, input_to_console, employee_file_writer, billable_client_list, validate)
    @messages_to_console = messages_to_console
    @messages = messages
    @input_to_console = input_to_console
    @employee_file_writer = employee_file_writer
    @billable_client_list = billable_client_list
    @validate = validate
  end

  def get_user_input
    @answer = input_to_console.get_user_input
  end

  def run
  messages_to_console.welcome_user(messages)
  loop do
    if answer == "2" then
      display_filtered_results
      messages_to_console.display_main_menu_options(messages)
      get_user_input
    end
    if answer == "3" then
      break
    end
    get_user_answers
    break if timecode.empty?
    if timecode.downcase == "billable work" then
      set_billable_client(timecode)
    else
      employee_file_writer.write_to_file(date, username, hours_worked, timecode)
      messages_to_console.display_main_menu_options(messages)
      get_user_input
    end
      break if answer.empty?
    end
  end

private

  def get_user_answers
    messages_to_console.request_username(messages)
    @username = input_to_console.get_user_input
    messages_to_console.request_date(messages)
    @date = input_to_console.get_user_input
    messages_to_console.request_worked_hours(messages)
    @hours_worked = input_to_console.get_user_input
    messages_to_console.request_timecode(messages)
    @timecode = input_to_console.get_user_input
  end

  def display_filtered_results
    messages_to_console.display_total_pto_hours(messages)
    messages_to_console.display_total_billable_hours(messages)
    messages_to_console.display_total_non_billable_hours(messages)
    messages_to_console.display_hours_worked_on_andys_eatery(messages)
    messages_to_console.display_hours_worked_on_cyrus_physics_outlet(messages)
    messages_to_console.display_hours_worked_on_erics_emporium_board_game(messages)
    messages_to_console.display_hours_worked_on_katrinas_armory(messages)
    messages_to_console.display_hours_worked_on_pixel_perfect(messages)
    messages_to_console.display_hours_worked_on_tams_deluxe_dancewear(messages)
  end

  def set_billable_client(timecode)
    if timecode.downcase == "billable work" then
      messages_to_console.request_client(messages)
      billable_client_list.read_billable_client_list
      @client = input_to_console.get_user_input
      employee_file_writer.write_to_file(date, username, hours_worked, timecode, client)
      messages_to_console.display_main_menu_options(messages)
      get_user_input
    end
  end
end
