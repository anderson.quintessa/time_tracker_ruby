require_relative 'time_tracker_runner'
require_relative 'messages_to_console'
require_relative 'messages'
require_relative 'input_to_console'
require_relative 'employee_file_writer'
require_relative 'billable_client_list'
require_relative 'validate'

time_tracker_runner = TimeTrackerRunner.new(MessagesToConsole.new, Messages.new, InputToConsole.new, EmployeeFileWriter.new, BillableClientList.new, Validate.new)
time_tracker_runner.run
