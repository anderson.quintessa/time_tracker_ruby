require_relative 'messages'
require_relative 'messages_to_console'

class InputToConsole

  def get_user_input
    gets.chomp.downcase
  end
end
