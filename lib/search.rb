class Search

  attr_accessor :results, :stored_username, :client_name

  PTO_ONLY_SEARCH = 0
  NON_BILLABLE_ONLY_SEARCH = 1
  BILLABLE_ONLY_SEARCH = 2
  FILTER_BY_NAME = 3
  FILTER_BY_ANDYS_EATERY = 4
  FILTERY_BY_CYRUS_PHYSICS_OUTLET = 5
  FILTER_BY_ERICS_BOARD_GAME_EMPORIUM = 6
  FILTER_BY_KATRINAS_ARMORY = 7
  FILTER_BY_PIXEL_PERFECT = 8
  FILTER_BY_TAMS_DELUXE_DANCEWEAR = 9

  def initialize(search_type)
    @results = []
    @stored_username = []
    @client_name = []
    # neeed to initialize time tracker runner class to have access to the username variable
  end

  def reads_all_employees_data_from_file
     IO.foreach("employee_data.txt") do |line|
      @entry = line.split("|")
     end
   end

  def search_type_cases(search_type)
    case search_type
    when 0 then get_hours_by_type(entry, username = TimeTrackerRunner.new.username, "PTO")
    when 1 then get_hours_by_type(entry, username, "Non-billable work")
    when 2 then get_hours_by_type(entry, username, "Billable work")
    when 3 then filter_by_name(@entry, username)
    when 4 then get_hours_worked_on_andys_eatery(entry, username, client_name)
    else
      get_hours_per_client(entry, username, client_name)
    end
  end

  def get_pto_hours_only(entry, username)
    get_hours_by_type(entry, username, "PTO")
  end

  def get_non_billable_hours_only(entry, username)
    get_hours_by_type(entry, username, "Non-billable work")
  end

  def get_billable_hours_only(entry, username)
    get_hours_by_type(entry, username, "Billable work")
  end

  def get_hours_by_type(entry, username, type_of_hours)
    entry.each { |val|
      stored_username = val[1]
      if val[1].downcase == username.downcase && val[2] >= "0" && val[3] == type_of_hours
        @results += [val[2].to_i]
      end
    }
    @results.sum
  end

  def get_hours_worked_on_andys_eatery(entry, username, client_name)
    get_hours_per_client(entry, username, client_name)
  end

  def get_hours_worked_on_cyrus_physics_outlet(entry, username, client_name)
    get_hours_per_client(entry, username, client_name)
  end

  def get_hours_worked_on_erics_emporium_board_game(entry, username, client_name)
    get_hours_per_client(entry, username, client_name)
  end

  def get_hours_worked_on_katrinas_armory(entry, username, client_name)
    get_hours_per_client(entry, username, client_name)
  end

  def get_hours_worked_on_pixel_perfect(entry, username, client_name)
    get_hours_per_client(entry, username, client_name)
  end

  def get_hours_worked_on_tams_deluxe_dancewear(entry, username, client_name)
    get_hours_per_client(entry, username, client_name)
  end

  def get_hours_per_client(entry, username, client_name)
    entry.each { |val|
      stored_username = val[1]
      client_name = val[4]
      if val[1].downcase == username.downcase && val[4].downcase == client_name.downcase && val[2] >= "0" && val[3] == "Billable work"
        @results += [val[2].to_i]
      end
    }
    @results.sum
  end

  def filter_by_name(entry, username)
    entry.each { |val|
      stored_username = val[1]
      if val[1].downcase == username.downcase
        results << val
      end
    }
    results
  end

end
